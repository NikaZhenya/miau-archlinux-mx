# Sitio miau.archlinux.mx

Este es un simple sitio hecho con puro HTML estático. Nada fancy.

Accede al sitio en:

* [miau.archlinux.mx](https://miau.archlinux.mx)
* [nikazhenya.gitlab.io/miau-archlinux-mx](https://nikazhenya.gitlab.io/miau-archlinux-mx)

## Licencia

<a href="https://es.wikipedia.org/wiki/Kopimismo">

![Kopimi](https://upload.wikimedia.org/wikipedia/commons/thumb/4/4b/Kopimi_rainbow.svg/320px-Kopimi_rainbow.svg.png)

</a>
